# Storage for Leica galvo stage insert

## Project structure

```bash
This project
    ├── assets # create new folders if needed
    │   ├── drawings # technical drawings
    │   ├── images # pictures or screenshots of the parts
    │   └── models # 2d/3d rendering of parts
    └── src # actual .stl source files
```

## One sentence pitch
![](assets/images/Storage_for_Leica_galvo_stage_insert_on_optical_table.jpeg)

Storage platform for a Leica galvo stage insert

## How it works
Stage inserts, when not mounted on the microscope stage, have to be stored safely to avoid damage due to swiping them off surfaces. A simple platform was designed to store the insert safely when not in use. The platform can be attached to an optical table, or screwed to shelves. The design can be adapted for different stage inserts, potentially from other companies.

## Authors
Gabriel Krens, Imaging and Optics Facility (IOF), IST Austria, iof@ista.ac.at.

Astrit Arslani, Machine Shop (MIBA), IST Austria, mshop@ist.ac.at.

## Acknowledgment
The MIBA Machine Shop of the IST Austria for production of the object, and allowing us to make it available for the public

